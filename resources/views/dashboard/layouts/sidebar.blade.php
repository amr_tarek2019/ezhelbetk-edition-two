<div class="page-sidebar">
    <div class="main-header-left d-none d-lg-block">
        <div class="logo-wrapper"><a href="index.html"><img src="{{ asset('assets/dashboard/images/logo.png') }}" style="margin-left: 50px" width="100px" alt=""></a></div>
    </div>
    <div class="sidebar custom-scrollbar">
        <div class="sidebar-user text-center">
            <div><img class="img-60 rounded-circle" src="{{ asset(Auth::user()->image) }}" alt="#">
                <div class="profile-edit"><a href="{{route('profile')}}" target="_blank"><i data-feather="edit"></i></a></div>
            </div>
            <h6 class="mt-3 f-14">{{Auth::user()->name}}</h6>
            <p>{{Auth::user()->email}}</p>
        </div>
        <ul class="sidebar-menu">
            <li><a class="sidebar-header" href="{{route('dashboard')}}"><i data-feather="home"></i>
                    <span>Dashboard</span>
                  </a>
            </li>
            <li><a class="sidebar-header" href="{{route('users.index')}}"><i data-feather="users"></i><span>users</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('admins.index')}}"><i data-feather="users"></i><span>admins</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('members.index')}}"><i data-feather="users"></i><span>members</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('technicians.index')}}"><i data-feather="users"></i><span>technicians</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('cities.index')}}"><i data-feather="map-pin"></i><span>cities</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('categories.index')}}"><i data-feather="layers"></i><span>categories</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('units.index')}}"><i data-feather="list"></i><span>units</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('subcategories.index')}}"><i data-feather="list"></i><span>subcategories</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('packages.index')}}"><i data-feather="box"></i><span>packages</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('subscriptions.index')}}"><i data-feather="box"></i><span>subscriptions</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('users.subscriptions.index')}}"><i data-feather="share-2"></i><span>Users subscriptions</span></a>
            </li>
            <li><a class="sidebar-header"><i data-feather="star"></i><span>ratings</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('users.rates.index')}}"><i class="fa fa-circle"></i>users</a></li>
                    <li><a href="{{route('technicians.rates.index')}}"><i class="fa fa-circle"></i>technicians</a></li>
                </ul>
            </li>

            <li><a class="sidebar-header" href="{{route('orders.index')}}"><i data-feather="tag"></i><span>orders</span></a>
            </li>


            <li><a class="sidebar-header"><i data-feather="users"></i><span>Staffs</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{ route('staffs.index') }}"><i class="fa fa-circle"></i>All staffs</a></li>
                    <li><a href="{{route('roles.index')}}"><i class="fa fa-circle"></i>Staff permissions</a></li>
                </ul>
            </li>

            <li><a class="sidebar-header" href="{{route('suggestions.index')}}"><i data-feather="mail"></i><span>suggestions</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('settings')}}"><i data-feather="settings"></i><span>settings</span></a>
            </li>
        </ul>
    </div>
</div>
