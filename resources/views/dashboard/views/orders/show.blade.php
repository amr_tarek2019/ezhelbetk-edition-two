@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>order data</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">orders</li>
                                <li class="breadcrumb-item active">order data</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>order data</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>payment</th>
                                        <th>city</th>
                                        <th>image</th>
                                        <th>note</th>
                                        <th>date</th>
                                        <th>time</th>
                                        <th>area</th>
                                        <th>block</th>
                                        <th>street</th>
                                        <th>house</th>
                                        <th>floor</th>
                                        <th>appartment</th>
                                        <th>directions</th>
                                        <th>status</th>
                                        <th>accepted</th>
                                        <th>order number</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <td>     @if($order->payment == true)
                                            <span class="label label-info">cash</span>
                                        @else
                                            <span class="label label-danger">paypal</span>
                                        @endif</td>
                                    <td>{{$order->city->city_en}}</td>
                                    <td><img class="img-fluid img-60" src="{{ $order->image}}" alt="#" data-original-title="" title=""></td>
                                    <td>{{$order->note}}</td>
                                    <td>{{$order->date}}</td>
                                    <td>{{$order->time}}</td>
                                    <td>{{$order->area}}</td>
                                    <td>{{$order->block}}</td>
                                    <td>{{$order->street}}</td>
                                    <td>{{$order->house}}</td>
                                    <td>{{$order->floor}}</td>
                                    <td>{{$order->appartment}}</td>
                                    <td>{{$order->directions}}</td>
                                    <td>
                                    @if($order->status == null)
                                        <span class="label label-info">not viewed</span>
                                    @elseif($order->status == 1)
                                        <span class="label label-success">not accepted</span>
                                    @else
                                        <span class="label label-danger">accepted</span>
                                    @endif
                                    </td>
                                    <td>
                                        @if($order->accepted == true)
                                            <span class="label label-info">accepted</span>
                                        @else
                                            <span class="label label-danger">refused</span>
                                        @endif

                                    </td>
                                    <td>{{$order->order_number}}</td>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>user Details</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>

                                        <th>name</th>
                                        <th>email</th>
                                        <th>image</th>
                                        <th>phone</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <td> <a href="{{route('users.show',$order->user_id)}}" data-original-title="" title="">{{ $order->user->name}}</a></td>
                                    <td>{{ $order->user['email']}}</td>
                                    <td><img class="img-fluid img-60" src="{{ $order->user->image}}" alt="#" data-original-title="" title=""></td>
                                    <td>{{ $order->user->phone}}</td>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>order Quantity And Total</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>quantity</th>
                                        <th>total</th>
                                    </tr>
                                    </thead>
                                    @foreach($orderDetails as $orderDetail)
                                    <tbody>
<td>{{$orderDetail->quantity}}</td>
<td>{{$orderDetail->total}}</td>
                                    </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>





                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>order Subcategories</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>Subcategories</th>
                                    </tr>
                                    </thead>
                                    @foreach($subcategories as $subcategory)
                                        <tbody>
                                        <td>{{$subcategory->name_en}}
{{--                                        <br>--}}
{{--                                            {{$subcategory->name_ar}}--}}
                                        </td>
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>






@endsection
