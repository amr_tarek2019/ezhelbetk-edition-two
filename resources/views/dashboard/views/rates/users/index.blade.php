@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Users Rates DataTable</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">rates</li>
                                <li class="breadcrumb-item">users</li>
                                <li class="breadcrumb-item active">Users Rates DataTable</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>Users Rates Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>order number</th>
                                        <th>rate</th>
                                        <th>feedback</th>
                                        <th>Created At</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($usersRates as $key=>$userRate)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$userRate->order->order_number}}</td>
                                            <td>{{$userRate->rate}}</td>
                                            <td>{{$userRate->review}}</td>
                                            <td>{{$userRate->created_at}}</td>
                                            <td>
                                                <a href="{{ route('users.rates.show',$userRate->id) }}" class="btn btn-info">Details</a>

                                                <form id="delete-form-{{ $userRate->id }}" action="{{ route('users.rates.destroy',$userRate->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('Are you sure ? You want to delete this field ?')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $userRate->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">delete</button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>





@endsection
