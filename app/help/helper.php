<?php

function unreadMsg()
{
    return \App\Contact::where('view',0)->get();
}

function countUnreadMsg()
{
    return \App\Contact::where('view',0)->count();
}


function unActiveUser()
{
    return \App\User::where('user_status',0)->where('user_type','user')->get();
}

function countUnActiveUser()
{
    return \App\User::where('user_status',0)->where('user_type','user')->count();
}
