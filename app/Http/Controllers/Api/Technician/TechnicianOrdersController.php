<?php

namespace App\Http\Controllers\Api\Technician;

use App\Category;
use App\Http\Controllers\Api\BaseController;
use App\JobDetails;
use App\Order;
use App\OrderSubcategory;
use App\Subcategory;
use App\TechnicalReport;
use App\Technician;
use App\TechnicianOrder;
use App\Unit;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\PushNotification;

class TechnicianOrdersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function nextVisit(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user=User::where('jwt_token',$jwt)->where('user_type','technician')->first();
        $technician=Technician::where('user_id',$user->id)->pluck('id')->first();

        $nextVisitOrders=TechnicianOrder::where('technician_id',$technician)->where('status','0')->get();
        $res_item = [];
        $res_list  = [];
        foreach ($nextVisitOrders as $res) {
            $res_item['id'] = $res->id;
            $res_item['order_number'] = $res->order_number;
            $res_item['status'] = $res->status;
            $getRequestDateTime=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $timeAndDate=Order::where('id',$getRequestDateTime)->select('time','date')->first();
            $res_item['time'] = $timeAndDate->time;
            $res_item['date'] = $timeAndDate->date;

            $techOrderId=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $subcategoryId=OrderSubcategory::where('order_id',$techOrderId)->pluck('subcategory_id')->first();
            $unitId=Subcategory::where('id',$subcategoryId)->pluck('unit_id')->first();
            $categoryId=Unit::where('id',$unitId)->pluck('category_id')->first();
            $getCategoryName=Category::where('id',$categoryId)->select('name_'.$lang.' as name')->first();
            $res_item['category_name'] = $getCategoryName->name;


            $getUserName=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $userName=Order::where('id',$getUserName)->pluck('user_id')->first();
            $user=User::where('id',$userName)->select('name')->first();
            $res_item['user_name'] = $user->name;


            $res_list[] = $res_item;
        }

        $response = [
            'message' => 'get data of available orders successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inProgress(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user=User::where('jwt_token',$jwt)->where('user_type','technician')->first();
        $technician=Technician::where('user_id',$user->id)->pluck('id')->first();

        $nextVisitOrders=TechnicianOrder::where('technician_id',$technician)->where('status','1')->orWhere('status','2')->orWhere('status','3')->orWhere('status','4')->get();
        $res_item = [];
        $res_list  = [];
        foreach ($nextVisitOrders as $res) {
            $res_item['id'] = $res->id;
            $res_item['order_number'] = $res->order_number;
            $res_item['status'] = $res->status;
            $getRequestDateTime=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $timeAndDate=Order::where('id',$getRequestDateTime)->select('time','date')->first();
            $res_item['time'] = $timeAndDate->time;
            $res_item['date'] = $timeAndDate->date;

            $techOrderId=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();

            $subcategoryId=OrderSubcategory::where('order_id',$techOrderId)->pluck('subcategory_id')->first();

            $unitId=Subcategory::where('id',$subcategoryId)->pluck('unit_id')->first();

            $categoryId=Unit::where('id',$unitId)->pluck('category_id')->first();

            $getCategoryName=Category::where('id',$categoryId)->select('name_'.$lang.' as name')->first();

            $res_item['category_name'] = $getCategoryName->name;


            $getUserName=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $userName=Order::where('id',$getUserName)->pluck('user_id')->first();
            $user=User::where('id',$userName)->select('name')->first();
            $res_item['user_name'] = $user->name;


            $res_list[] = $res_item;
        }

        $response = [
            'message' => 'get data of available orders successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function completed(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user=User::where('jwt_token',$jwt)->where('user_type','technician')->first();
        $technician=Technician::where('user_id',$user->id)->pluck('id')->first();

        $nextVisitOrders=TechnicianOrder::where('technician_id',$technician)->where('status','5')->get();
        $res_item = [];
        $res_list  = [];
        foreach ($nextVisitOrders as $res) {
            $res_item['id'] = $res->id;
            $res_item['order_number'] = $res->order_number;
            $res_item['status'] = $res->status;
            $getRequestDateTime=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $timeAndDate=Order::where('id',$getRequestDateTime)->select('time','date')->first();
            $res_item['time'] = $timeAndDate->time;
            $res_item['date'] = $timeAndDate->date;

            $techOrderId=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $subcategoryId=OrderSubcategory::where('order_id',$techOrderId)->pluck('subcategory_id')->first();
            $unitId=Subcategory::where('id',$subcategoryId)->pluck('unit_id')->first();
            $categoryId=Unit::where('id',$unitId)->pluck('category_id')->first();
            $getCategoryName=Category::where('id',$categoryId)->select('name_'.$lang.' as name')->first();
            $res_item['category_name'] = $getCategoryName->name;


            $getUserName=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $userName=Order::where('id',$getUserName)->pluck('user_id')->first();
            $user=User::where('id',$userName)->select('name')->first();
            $res_item['user_name'] = $user->name;


            $res_list[] = $res_item;
        }

        $response = [
            'message' => 'get data of available orders successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function orderDetails(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';

        $orderDetails= TechnicianOrder::where('id',$request->id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($orderDetails as $res) {
            $res_item['id'] = $res->id;
            $res_item['order_number'] = $res->order_number;
            $res_item['status'] = $res->status;
            $getRequestDateTime=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $timeAndDate=Order::where('id',$getRequestDateTime)->select('time','date')->first();
            $res_item['time'] = $timeAndDate->time;
            $res_item['date'] = $timeAndDate->date;
            if(!empty($res->note)) {
                $res_item['note'] = $timeAndDate->note;
            }else{
                $res_item['note']='';
            }
            if(!empty($res->image)) {
                $res_item['note_image'] = $timeAndDate->image;
            }else{
                $res_item['note_image']='';
            }

            $techOrderId=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $subcategoryId=OrderSubcategory::where('order_id',$techOrderId)->pluck('subcategory_id')->first();
            $unitId=Subcategory::where('id',$subcategoryId)->pluck('unit_id')->first();
            $categoryId=Unit::where('id',$unitId)->pluck('category_id')->first();
            $getCategoryName=Category::where('id',$categoryId)->select('name_'.$lang.' as name')->first();
//            $res_item['category_name'] = $getCategoryName->name;


            $getUserName=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $userName=Order::where('id',$getUserName)->pluck('user_id')->first();
            $user=User::where('id',$userName)->select('lat','lng')->first();
            $res_item['user_lat'] = $user->lat;
            $res_item['user_lng'] = $user->lng;

            $subId=OrderSubcategory::where('order_id',$techOrderId)->pluck('subcategory_id')->first();
            $subName=Subcategory::where('id',$subId)->select('name_'.$lang.' as name')->first();
//            $res_item['subcategory_name'] = $subName->name;
            $res_item['issue']=$getCategoryName->name.' , '.$subName->name;


            $getOrderDetails=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $order=Order::where('id',$getOrderDetails)->select('street','area','block','house','payment_type','currency')->first();
            $res_item['street']=$order->street;
            $res_item['area']=$order->area;
            $res_item['block']=$order->block;
            $res_item['house']=$order->house;
            $res_item['payment_type']=$order->payment_type;
            $res_item['currency']=$order->currency;

            // $techOrderIdNumber=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            // $subcategoryPrice=OrderSubcategory::where('order_id',$techOrderIdNumber)->select('total')->get();
            // $res_item['subcategory_price']=$subcategoryPrice;

            $getSumOfOrder=OrderSubcategory::where('order_id',$res->order_id)->select('total')->sum('total');
            $res_item['total']=(string)$getSumOfOrder;


            $res_list = $res_item;
        }
        $response = [
            'message' => 'get data of order details successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function acceptOrderForm(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';

        $orderDetails= TechnicianOrder::where('id',$request->id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($orderDetails as $res) {
            $res_item['id'] = $res->id;
            $res_item['order_number'] = $res->order_number;
            $res_item['status'] = $res->status;
            $getUserName=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $userName=Order::where('id',$getUserName)->pluck('user_id')->first();
            $user=User::where('id',$userName)->select('lat','lng','address','phone','name')->first();
            $res_item['user_lat'] = $user->lat;
            $res_item['user_lng'] = $user->lng;
            $res_item['user_name'] = $user->name;
            $res_item['user_address'] = $user->address;
            $res_item['user_phone']=$user->phone;
            $technician=Technician::where('id',$res->technician_id)->pluck('user_id')->first();
            $userTechnician=User::where('id',$technician)->select('lat','lng','address')->first();
            $res_item['technician_lat'] = $userTechnician->lat;
            $res_item['technician_lng'] = $userTechnician->lng;
            $res_item['technician_address'] = $userTechnician->address;

            $getSumOfOrder=OrderSubcategory::where('order_id',$res->order_id)->select('total')->sum('total');
            $res_item['total']=(string)$getSumOfOrder;

            $getOrderNotesAndImage=Order::where('id',$getUserName)->select('note','image','currency')->first();
            $res_item['order_image'] = $getOrderNotesAndImage->image;
            $res_item['order_notes'] = $getOrderNotesAndImage->note;
            $res_item['currency'] = $getOrderNotesAndImage->currency;

            $unit='K';
            $theta = $userTechnician->lng - $user->lng;
            $dist = sin(deg2rad( $userTechnician->lat)) * sin(deg2rad($user->lat)) +  cos(deg2rad($userTechnician->lat)) * cos(deg2rad($user->lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles =(string) round($dist * 60 * 1.1515);
            $unit = strtoupper($unit);
            if ($unit == "K")
            {
                $res_item['distance'] = (string)round($miles * 1.609344,2). " KM";
            }
            else if ($unit == "N")
            {
                $res_item['distance'] =(string) round($miles * 0.8684,2);
            }
            else {
                $res_item['distance'] =(string) round($miles,2);
            }


            $res_list = $res_item;
        }
        $response = [
            'message' => 'get data of order details successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function replyOnOrder(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->pluck('id')->first();
        if (!$user) {

            return $response=[
                'success'=>403,
                'message'=>'please login first',
            ];
        }
        $validator = Validator::make($request->all(), [
            'accepted'=>'required',
        ]);
        if ($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $orderReply=TechnicianOrder::find($request->id);
        $orderReply->accepted = $request->accepted;
        $orderReply->status = '1';
        $orderReply->save();
        // PushNotification::send($token,'technician replied on order',1);

        $response=[
            'message'=>'response sent successfully',
            'status'=>'200',
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeOrderStatus(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->pluck('id')->first();
        if (!$user) {

            return $response=[
                'success'=>403,
                'message'=>'please login first',
            ];
        }
        $validator = Validator::make($request->all(), [
            'status'=>'required',
        ]);
        if ($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $orderReply=TechnicianOrder::find($request->id);
        $orderReply->status = $request->status;
        $orderReply->save();
        // PushNotification::send($token,'order status changed successfully',1);
        $response=[
            'message'=>'response sent successfully',
            'status'=>'200',
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }




}
