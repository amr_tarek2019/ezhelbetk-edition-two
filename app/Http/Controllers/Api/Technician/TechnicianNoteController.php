<?php

namespace App\Http\Controllers\Api\Technician;

use App\Category;
use App\Http\Controllers\Api\BaseController;
use App\Order;
use App\OrderRate;
use App\OrderRequest;
use App\OrderSubcategory;
use App\Subcategory;
use App\Technician;
use App\TechnicianNote;
use App\TechnicianOrder;
use App\Unit;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TechnicianNoteController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createNote(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->pluck('id')->first();
        $technician=Technician::where('user_id',$user)->select('id')->first();
        $order=Order::where('id',$request->order_id)->first();
        if (!$user) {
            return $response=[
                'success'=>403,
                'message'=>'please login first',
            ];
        }

        $validator = Validator::make($request->all(), [
            'know_price'=>'required',
            'order_id'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $technicanNote=new TechnicianNote();
        $technicanNote->technician_id=$technician->id;
        $technicanNote->order_id=$order->id;
        $technicanNote->know_price = $request->know_price;
        $technicanNote->price = $request->price;
        $technicanNote->save();
        $response=[
            'message'=>'technical note created successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateNote(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->pluck('id')->first();
        if (!$user) {
            return $response=[
                'success'=>403,
                'message'=>'please login first',
            ];
        }
        $validator = Validator::make($request->all(), [
            'know_price'=>'required',
            'price' => 'required',

        ]);
        if ($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $noteResult=TechnicianNote::find($request->id);
        $noteResult->know_price = $request->know_price;
        $noteResult->price = $request->price;
        $noteResult->save();
        $response=[
            'message'=>'technician note updated successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function waitingOrders(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user=User::where('jwt_token',$jwt)->where('user_type','technician')->first();
        $technician=Technician::where('user_id',$user->id)->pluck('id')->first();

        $waitingOrders=TechnicianNote::where('technician_id',$technician)->where('know_price','0')->get();

        $res_item = [];
        $res_list  = [];
        foreach ($waitingOrders as $res) {
            $res_item['id'] = $res->id;
            $getRequestDateTime=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $timeAndDate=Order::where('id',$getRequestDateTime)->select('time','date')->first();
            $res_item['time'] = $timeAndDate->time;
            $res_item['date'] = $timeAndDate->date;

            $techOrderId=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $subcategoryId=OrderSubcategory::where('order_id',$techOrderId)->pluck('subcategory_id')->first();
            $unitId=Subcategory::where('id',$subcategoryId)->pluck('unit_id')->first();
            $categoryId=Unit::where('id',$unitId)->pluck('category_id')->first();
            $getCategoryName=Category::where('id',$categoryId)->select('name_'.$lang.' as name')->first();
            $res_item['category_name'] = $getCategoryName->name;


            $getUserName=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $userName=Order::where('id',$getUserName)->pluck('user_id')->first();
            $user=User::where('id',$userName)->select('name')->first();
            $res_item['user_name'] = $user->name;

            $order=TechnicianOrder::where('order_id',$res->order_id)->select('order_number','status')->first();
            $res_item['order_number'] = $order->order_number;

            $res_list[] = $res_item;
        }

        $response = [
            'message' => 'get data of waiting orders successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
