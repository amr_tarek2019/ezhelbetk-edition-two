<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Order;
use App\OrderSubcategory;
use App\Subcategory;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('dashboard.views.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $orderDetails=OrderSubcategory::where('order_id',$id)->get();

        $data = Order::where('id',$id)->pluck('id');
        $subcategory_id = OrderSubcategory::whereIn('order_id',$data)->pluck('subcategory_id');
        $subcategories= Subcategory::whereIn('id',$subcategory_id)->get();

        return view('dashboard.views.orders.show',compact('order','orderDetails','subcategories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $technicians=User::where('user_type','technician')->get();
        $order = Order::find($id);
        return view('dashboard.views.orders.edit',compact('order','technicians'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'status' => 'required',
            'technician' => 'required',
            'total_price'=>'required',
        ]);
        $order=Order::where('id',$id)->first();
        $order->status=$request->status;
        $order->total_price=$request->total_price;
        $order->save();

        // $user=\App\Request::where('user_id',$id)->first();

        $reservation=New OrderRequest();
//        $reservation->order_id=$order_id;
        $reservation->user_id=$request->user_id;
        $reservation->order_id=$order->id;
        $reservation->technician_id=$request->technician;
        $reservation->order_number=$order->order_number;
        $reservation->status= $order->status;
        $reservation->order_type= $request->order_type;
        //dd($request->all());
        $reservation->save();



        return redirect()->route('orders.index')->with('successMsg','Order Successfully Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order= Order::find($id);
        $order->delete();
        return redirect()->route('orders.index')->with('successMsg','Order Successfully Deleted');
    }


}
