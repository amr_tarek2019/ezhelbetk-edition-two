<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Unit;
use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = Subcategory::all();
        return view('dashboard.views.subcategories.index', compact('subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $units = Unit::all();
        return view('dashboard.views.subcategories.create',compact('units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'unit' => 'required',
            'name_en' => 'required',
            'name_ar' => 'required',
            'details_en' => 'required',
            'details_ar' => 'required',
            'price' => 'required',
        ]);
        $subcategory = new Subcategory();
        $subcategory->unit_id = $request->unit;
        $subcategory->name_en = $request->name_en;
        $subcategory->name_ar = $request->name_ar;
        $subcategory->details_en = $request->details_en;
        $subcategory->details_ar = $request->details_ar;
        $subcategory->price = $request->price;
        $subcategory->save();
        return redirect()->route('subcategories.index')->with('successMsg','Profile Updated Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategory = Subcategory::find($id);
        $units = Unit::all();
        return view('dashboard.views.subcategories.edit',compact('units','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subcategory = Subcategory::find($id);
        $subcategory->unit_id = $request->unit;
        $subcategory->name_en = $request->name_en;
        $subcategory->name_ar = $request->name_ar;
        $subcategory->details_en = $request->details_en;
        $subcategory->details_ar = $request->details_ar;
        $subcategory->price = $request->price;
        $subcategory->save();
        return redirect()->route('subcategories.index')->with('successMsg','Profile Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory = Subcategory::find($id);
        $subcategory->delete();
        return redirect()->route('subcategories.index')->with('successMsg','Profile Updated Successfully');
    }

    public function updateStatus(Request $request)
    {
        $subcategory = Subcategory::findOrFail($request->id);
        $subcategory->status = $request->status;
        if($subcategory->save()){
            return 1;
        }
        return 0;
    }


}
