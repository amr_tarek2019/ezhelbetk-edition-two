<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderPriceAmount extends Model
{
    protected $table='order_price_amount';
    protected $fillable=['order_id', 'price_amount'];
}
